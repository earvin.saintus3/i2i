<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, $this->getConfiguration('Prénom', 'Votre prénom !',['attr' => ['class' => "form-control",]]))
            ->add('lastName', TextType::class, $this->getConfiguration('Nom', 'Votre nom de famille !',['attr' => ['class' => "form-control",]]))
            ->add('email', EmailType::class, $this->getConfiguration('Email', 'Votre adresse mail !',['attr' => ['class' => "form-control",]]))
            ->add('picture', UrlType::class, $this->getConfiguration('Photo de  profil', 'URL de votre avatar ...',['attr' => ['class' => "form-control",]]))
            ->add('hash', PasswordType::class, $this->getConfiguration('Mot de passe', 'Choisissez un bon mot de passe !',['attr' => ['class' => "form-control",]]))
            ->add('passwordConfirm', PasswordType::class, $this->getConfiguration('Confirmation de mot de passe', 'Confirmer votre mot de passe !',['attr' => ['class' => "form-control",]]))
            ->add('introduction', TextType::class, $this->getConfiguration('Introduction', 'Présentez vous en quelques mots...',['attr' => ['class' => "form-control",]]))
            ->add('description', TextareaType::class, $this->getConfiguration('Description', "C'est le moment de vous présenter en détails ...",['attr' => ['class' => "form-control",]]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
