<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName',null, ['attr' => ['class' => "form-control"]])
            ->add('lastName',null, ['attr' => ['class' => "form-control"]])
            ->add('email',null, ['attr' => ['class' => "form-control"]])
            ->add('picture',null, ['attr' => ['class' => "form-control"]])
            ->add('introduction',null,['attr' => ['class' => "form-control"]])
            ->add('description',null, ['attr' => ['class' => "form-control"]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
