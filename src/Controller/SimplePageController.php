<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SimplePageController extends AbstractController
{
    /**
     * @Route("/contact", name="contact_us")
     *
     * @return Response
     */
    public function pageContact()
    {
        return $this->render(
            '/partials/contact.html.twig'
        );
    }

    /**
     * @Route("/conditions-generales-d-utilisation", name="cgu")
     *
     * @return Response
     */
    public function pageCGU()
    {
        return $this->render(
            '/partials/cgu.html.twig'
        );
    }
    
    /**
     * @Route("/politiques-de-confidentialite", name="pdc")
     *
     * @return Response
     */
    public function pagePDC()
    {
        return $this->render(
            '/partials/pdc.html.twig'
        );
    }

    /**
     * @Route("/aboutus", name="aboutus")
     *
     * @return Response
     */
    public function pageAbout()
    {
        return $this->render(
            '/partials/about.html.twig'
        );
    } 
}
