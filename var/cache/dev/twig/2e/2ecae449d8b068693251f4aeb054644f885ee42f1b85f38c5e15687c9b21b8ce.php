<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/footer.html.twig */
class __TwigTemplate_7ca88f4b05c9949d21193f0d01f8a580716f0553127108ed4b4940f13501bb70 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "partials/footer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "partials/footer.html.twig"));

        // line 1
        echo "<footer id=\"contact-us\">
    <div class=\"container\">
        <div class=\"footer-content\">
            <div class=\"row\">
                <!-- ***** Contact Form End ***** -->
                <div class=\"right-content col-lg-6 col-md-12 col-sm-12 cob\">
                    <h2>A propos de <em>i2i</em></h2>
                    <p class=\"cob\">Nous mettons en relation les utilisateurs et les propriétaires qui veulent profiter d'opportunités et gagner de l'argent en proposant leur matériel informatique.</p>
                    <ul class=\"social\">
                        <li><a href=\"https://fb.com/templatemo\"><i class=\"fa fa-facebook\"></i></a></li>
                        <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>
                        <li><a href=\"#\"><i class=\"fa fa-linkedin\"></i></a></li>
                        <li><a href=\"#\"><i class=\"fa fa-rss\"></i></a></li>
                        <li><a href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>
                    </ul>
                    <div class=\"linksUsefull\">
                        <ul>
                            <li><a href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pdc");
        echo "\">Politiques de confidentialité</a></li>
                            <li><a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cgu");
        echo "\">Conditions générale d'utilisations</a></li>
                        </ul>
                        <ul>
                            <li><a href=\"";
        // line 22
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("aboutus");
        echo "\">A propos</a></li>
                            <li><a href=\"";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_us");
        echo "\">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-12\">
                <div class=\"sub-footer\">
                    <p>Copyright &copy; 2021 High-tech to High-tech</p>
                </div>
            </div>
        </div>
    </div>
</footer>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "partials/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 23,  72 => 22,  66 => 19,  62 => 18,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<footer id=\"contact-us\">
    <div class=\"container\">
        <div class=\"footer-content\">
            <div class=\"row\">
                <!-- ***** Contact Form End ***** -->
                <div class=\"right-content col-lg-6 col-md-12 col-sm-12 cob\">
                    <h2>A propos de <em>i2i</em></h2>
                    <p class=\"cob\">Nous mettons en relation les utilisateurs et les propriétaires qui veulent profiter d'opportunités et gagner de l'argent en proposant leur matériel informatique.</p>
                    <ul class=\"social\">
                        <li><a href=\"https://fb.com/templatemo\"><i class=\"fa fa-facebook\"></i></a></li>
                        <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>
                        <li><a href=\"#\"><i class=\"fa fa-linkedin\"></i></a></li>
                        <li><a href=\"#\"><i class=\"fa fa-rss\"></i></a></li>
                        <li><a href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>
                    </ul>
                    <div class=\"linksUsefull\">
                        <ul>
                            <li><a href=\"{{ path('pdc') }}\">Politiques de confidentialité</a></li>
                            <li><a href=\"{{ path('cgu') }}\">Conditions générale d'utilisations</a></li>
                        </ul>
                        <ul>
                            <li><a href=\"{{ path('aboutus') }}\">A propos</a></li>
                            <li><a href=\"{{ path('contact_us') }}\">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-12\">
                <div class=\"sub-footer\">
                    <p>Copyright &copy; 2021 High-tech to High-tech</p>
                </div>
            </div>
        </div>
    </div>
</footer>", "partials/footer.html.twig", "C:\\Users\\esaintus\\Documents\\projetCours\\i2i\\templates\\partials\\footer.html.twig");
    }
}
