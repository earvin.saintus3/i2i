<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ad/_adHome.html.twig */
class __TwigTemplate_9c2fe69f80851a0ae3f0b1e8d7e01ab8b00b71daa9324f3976613f5e7b5a4c61 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ad/_adHome.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ad/_adHome.html.twig"));

        // line 1
        $context["url"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ads_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 1, $this->source); })()), "slug", [], "any", false, false, false, 1)]);
        // line 2
        echo "<div class=\"item service-item\">
    <div class=\"testimonial-content\">
        <ul class=\"stars\">
            <strong>";
        // line 5
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 5, $this->source); })()), "price", [], "any", false, false, false, 5), 2, ",", " "), "html", null, true);
        echo "&euro; / jour</strong> <br>
            ";
        // line 6
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 6, $this->source); })()), "comments", [], "any", false, false, false, 6)) > 0)) {
            // line 7
            echo "                ";
            $this->loadTemplate("partials/rating.html.twig", "ad/_adHome.html.twig", 7)->display(twig_array_merge($context, ["rating" => twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 7, $this->source); })()), "avgRatings", [], "any", false, false, false, 7)]));
            // line 8
            echo "            ";
        } else {
            // line 9
            echo "                <small>Pas encore noté !</small>
            ";
        }
        // line 11
        echo "        </ul>
        <a href=\"";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 12, $this->source); })()), "html", null, true);
        echo "\">
            <img src=\"";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 13, $this->source); })()), "coverImage", [], "any", false, false, false, 13), "html", null, true);
        echo "\" alt=\"Image du produit\" class=\"img-card\"/>
        </a>
        <h4>
            <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 16, $this->source); })()), "html", null, true);
        echo "\" class=\"color-white\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 16, $this->source); })()), "title", [], "any", false, false, false, 16), "html", null, true);
        echo "</a>
        </h4>
        <p>";
        // line 18
        echo twig_truncate_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 18, $this->source); })()), "introduction", [], "any", false, false, false, 18), 150);
        echo "</p>
        <a href=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 19, $this->source); })()), "html", null, true);
        echo "\" class=\"btn btn-dark\">
            En savoir plus !
        </a>
        ";
        // line 22
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 22, $this->source); })()), "user", [], "any", false, false, false, 22) && (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 22, $this->source); })()), "user", [], "any", false, false, false, 22) == twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 22, $this->source); })()), "author", [], "any", false, false, false, 22)))) {
            // line 23
            echo "            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ads_edit", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 23, $this->source); })()), "slug", [], "any", false, false, false, 23)]), "html", null, true);
            echo "\" class=\"btn btn-info\">Modifier l'annonce !</a>
        ";
        }
        // line 25
        echo "    </div>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ad/_adHome.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 25,  98 => 23,  96 => 22,  90 => 19,  86 => 18,  79 => 16,  73 => 13,  69 => 12,  66 => 11,  62 => 9,  59 => 8,  56 => 7,  54 => 6,  50 => 5,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set url = path('ads_show', {'slug': ad.slug}) %}
<div class=\"item service-item\">
    <div class=\"testimonial-content\">
        <ul class=\"stars\">
            <strong>{{ ad.price | number_format(2, ',', ' ') }}&euro; / jour</strong> <br>
            {% if ad.comments|length > 0 %}
                {% include 'partials/rating.html.twig' with {'rating':ad.avgRatings}%}
            {% else %}
                <small>Pas encore noté !</small>
            {% endif %}
        </ul>
        <a href=\"{{url}}\">
            <img src=\"{{ ad.coverImage }}\" alt=\"Image du produit\" class=\"img-card\"/>
        </a>
        <h4>
            <a href=\"{{url}}\" class=\"color-white\">{{ ad.title }}</a>
        </h4>
        <p>{{ ad.introduction | truncate(150) | raw }}</p>
        <a href=\"{{url}}\" class=\"btn btn-dark\">
            En savoir plus !
        </a>
        {% if app.user and app.user == ad.author %}
            <a href=\"{{path('ads_edit', {'slug':ad.slug})}}\" class=\"btn btn-info\">Modifier l'annonce !</a>
        {% endif %}
    </div>
</div>", "ad/_adHome.html.twig", "C:\\Users\\esaintus\\Documents\\projetCours\\i2i\\templates\\ad\\_adHome.html.twig");
    }
}
