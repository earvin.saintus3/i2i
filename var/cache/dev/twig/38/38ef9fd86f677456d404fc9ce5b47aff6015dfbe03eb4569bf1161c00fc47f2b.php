<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* booking/show.html.twig */
class __TwigTemplate_259bd70f9ae10474cbeb7f79ec1882292459d1e2d4c6ef4f7310f24413a8a766 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "booking/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "booking/show.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "booking/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Réservation n°";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["booking"]) || array_key_exists("booking", $context) ? $context["booking"] : (function () { throw new RuntimeError('Variable "booking" does not exist.', 2, $this->source); })()), "id", [], "any", false, false, false, 2), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        $context["ad"] = twig_get_attribute($this->env, $this->source, (isset($context["booking"]) || array_key_exists("booking", $context) ? $context["booking"] : (function () { throw new RuntimeError('Variable "booking" does not exist.', 4, $this->source); })()), "ad", [], "any", false, false, false, 4);
        // line 5
        $context["author"] = twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 5, $this->source); })()), "author", [], "any", false, false, false, 5);
        // line 6
        echo "    <div class=\"container mat7\">
        <h1 class=\"maTmaB tac\">Votre réservation (n°";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["booking"]) || array_key_exists("booking", $context) ? $context["booking"] : (function () { throw new RuntimeError('Variable "booking" does not exist.', 7, $this->source); })()), "id", [], "any", false, false, false, 7), "html", null, true);
        echo ")</h1>
        ";
        // line 8
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "request", [], "any", false, false, false, 8), "query", [], "any", false, false, false, 8), "get", [0 => "widthAlertSuccess"], "method", false, false, false, 8)) {
            // line 9
            echo "            <div class=\"alert alert-success\">
                <h4 class=\"alert-heading\">Bravo !</h4>
                <p>
                    Votre réservation auprès de
                    <strong>
                        <a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["author"]) || array_key_exists("author", $context) ? $context["author"] : (function () { throw new RuntimeError('Variable "author" does not exist.', 14, $this->source); })()), "slug", [], "any", false, false, false, 14)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["author"]) || array_key_exists("author", $context) ? $context["author"] : (function () { throw new RuntimeError('Variable "author" does not exist.', 14, $this->source); })()), "fullName", [], "any", false, false, false, 14), "html", null, true);
            echo "</a>
                    </strong>
                    pour l'annonce
                    <strong>
                        <a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ads_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 18, $this->source); })()), "slug", [], "any", false, false, false, 18)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 18, $this->source); })()), "title", [], "any", false, false, false, 18), "html", null, true);
            echo "</a>
                    </strong>
                </p>
            </div>
        ";
        }
        // line 23
        echo "        <div class=\"row\">
            <div class=\"alert alert-light\">
                <h2>Détails</h2>
                <dl class=\"row\">
                    <dt class=\"col-md-4\">Numéro</dt>
                    <dd class=\"col-md-8\">";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["booking"]) || array_key_exists("booking", $context) ? $context["booking"] : (function () { throw new RuntimeError('Variable "booking" does not exist.', 28, $this->source); })()), "id", [], "any", false, false, false, 28), "html", null, true);
        echo "</dd>
                    <dt class=\"col-md-4\">Date de prise en main</dt>
                    <dd class=\"col-md-8\">";
        // line 30
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["booking"]) || array_key_exists("booking", $context) ? $context["booking"] : (function () { throw new RuntimeError('Variable "booking" does not exist.', 30, $this->source); })()), "startDate", [], "any", false, false, false, 30), "d/m/Y"), "html", null, true);
        echo "</dd>
                    <dt class=\"col-md-4\">Date de retour</dt>
                    <dd class=\"col-md-8\">";
        // line 32
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["booking"]) || array_key_exists("booking", $context) ? $context["booking"] : (function () { throw new RuntimeError('Variable "booking" does not exist.', 32, $this->source); })()), "endDate", [], "any", false, false, false, 32), "d/m/Y"), "html", null, true);
        echo "</dd>
                    <dt class=\"col-md-4\">Nombre de jour</dt>
                    <dd class=\"col-md-8\">";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["booking"]) || array_key_exists("booking", $context) ? $context["booking"] : (function () { throw new RuntimeError('Variable "booking" does not exist.', 34, $this->source); })()), "duration", [], "any", false, false, false, 34), "html", null, true);
        echo "</dd>
                    <dt class=\"col-md-4\">Montant total</dt>
                    <dd class=\"col-md-8\">";
        // line 36
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["booking"]) || array_key_exists("booking", $context) ? $context["booking"] : (function () { throw new RuntimeError('Variable "booking" does not exist.', 36, $this->source); })()), "amount", [], "any", false, false, false, 36), 2, ",", " "), "html", null, true);
        echo "&euro;</dd>
                    <dt class=\"col-md-4\">COmmentaire</dt>
                    <dd class=\"col-md-8\">";
        // line 38
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "comment", [], "any", true, true, false, 38)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["booking"] ?? null), "comment", [], "any", false, false, false, 38), "Aucuns commentaires")) : ("Aucuns commentaires")), "html", null, true);
        echo "</dd>
                </dl> 
                <hr>
                <h2 class=\"tac alert-heading\">Votre client</h2>
                <div class=\"row mb-3\">
                    <div class=\"col-3\">
                        <img src=\"";
        // line 44
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["author"]) || array_key_exists("author", $context) ? $context["author"] : (function () { throw new RuntimeError('Variable "author" does not exist.', 44, $this->source); })()), "picture", [], "any", false, false, false, 44), "html", null, true);
        echo "\" class=\"avatar avatar-medium\" alt=\"Avatar de ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["author"]) || array_key_exists("author", $context) ? $context["author"] : (function () { throw new RuntimeError('Variable "author" does not exist.', 44, $this->source); })()), "fullName", [], "any", false, false, false, 44), "html", null, true);
        echo "\"/>
                    </div>
                    <div class=\"col-9\">
                        <h4>
                            <a href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["author"]) || array_key_exists("author", $context) ? $context["author"] : (function () { throw new RuntimeError('Variable "author" does not exist.', 48, $this->source); })()), "slug", [], "any", false, false, false, 48)]), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["author"]) || array_key_exists("author", $context) ? $context["author"] : (function () { throw new RuntimeError('Variable "author" does not exist.', 48, $this->source); })()), "fullName", [], "any", false, false, false, 48), "html", null, true);
        echo "</a>
                        </h4>
                        <span class=\"badge badge-primary\">";
        // line 50
        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["author"]) || array_key_exists("author", $context) ? $context["author"] : (function () { throw new RuntimeError('Variable "author" does not exist.', 50, $this->source); })()), "ads", [], "any", false, false, false, 50)), "html", null, true);
        echo " annonces</span>
                    </div>
                </div>
                ";
        // line 53
        echo twig_get_attribute($this->env, $this->source, (isset($context["author"]) || array_key_exists("author", $context) ? $context["author"] : (function () { throw new RuntimeError('Variable "author" does not exist.', 53, $this->source); })()), "description", [], "any", false, false, false, 53);
        echo "
                <a class=\"btn btn-primary\" href=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["author"]) || array_key_exists("author", $context) ? $context["author"] : (function () { throw new RuntimeError('Variable "author" does not exist.', 54, $this->source); })()), "slug", [], "any", false, false, false, 54)]), "html", null, true);
        echo "\">Plus d'infos sur ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["author"]) || array_key_exists("author", $context) ? $context["author"] : (function () { throw new RuntimeError('Variable "author" does not exist.', 54, $this->source); })()), "firstName", [], "any", false, false, false, 54), "html", null, true);
        echo "</a>
            </div>

            <div class=\"alet alert-light\" id=\"comment\">
                <h2 class=\"tac alert-heading\">Votre avis compte !</h2>
                ";
        // line 59
        if ((twig_date_converter($this->env) > twig_date_converter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["booking"]) || array_key_exists("booking", $context) ? $context["booking"] : (function () { throw new RuntimeError('Variable "booking" does not exist.', 59, $this->source); })()), "endDate", [], "any", false, false, false, 59)))) {
            // line 60
            echo "                    ";
            $context["comment"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["booking"]) || array_key_exists("booking", $context) ? $context["booking"] : (function () { throw new RuntimeError('Variable "booking" does not exist.', 60, $this->source); })()), "ad", [], "any", false, false, false, 60), "commentFromAuthor", [0 => twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 60, $this->source); })()), "user", [], "any", false, false, false, 60)], "method", false, false, false, 60);
            // line 61
            echo "                    ";
            if ( !(null === (isset($context["comment"]) || array_key_exists("comment", $context) ? $context["comment"] : (function () { throw new RuntimeError('Variable "comment" does not exist.', 61, $this->source); })()))) {
                // line 62
                echo "                        <blockquote>
                            ";
                // line 63
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["comment"]) || array_key_exists("comment", $context) ? $context["comment"] : (function () { throw new RuntimeError('Variable "comment" does not exist.', 63, $this->source); })()), "content", [], "any", false, false, false, 63), "html", null, true);
                echo "
                        </blockquote>
                        <strong>Note : </strong> ";
                // line 65
                $this->loadTemplate("partials/rating.html.twig", "booking/show.html.twig", 65)->display(twig_array_merge($context, ["rating" => twig_get_attribute($this->env, $this->source, (isset($context["comment"]) || array_key_exists("comment", $context) ? $context["comment"] : (function () { throw new RuntimeError('Variable "comment" does not exist.', 65, $this->source); })()), "rating", [], "any", false, false, false, 65)]));
                // line 66
                echo "                    ";
            } else {
                // line 67
                echo "                        ";
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 67, $this->source); })()), 'form_start');
                echo "
                        ";
                // line 68
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 68, $this->source); })()), 'widget');
                echo "
                        <button type=\"submit\" class=\"btn btn-success\">Confirmer !</button>
                        ";
                // line 70
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 70, $this->source); })()), 'form_end');
                echo "
                    ";
            }
            // line 72
            echo "                ";
        } else {
            // line 73
            echo "                <p>Vous ne pouvez pas noté cette annonce tant que vous n'avez pas rendu le produit !</p>
                ";
        }
        // line 75
        echo "            </div>

        </div>
        <div class=\"col\">
            <div class=\"alert alert-light\">
                <h2 class=\"tac alert-heading\">Votre location</h2>
                <h4>
                    <a href=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ads_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["author"]) || array_key_exists("author", $context) ? $context["author"] : (function () { throw new RuntimeError('Variable "author" does not exist.', 82, $this->source); })()), "slug", [], "any", false, false, false, 82)]), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 82, $this->source); })()), "title", [], "any", false, false, false, 82), "html", null, true);
        echo "</a>
                </h4>
                <img src=\"";
        // line 84
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 84, $this->source); })()), "coverImage", [], "any", false, false, false, 84), "html", null, true);
        echo "\" class=\"img-fluid\" alt=\"Image de ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 84, $this->source); })()), "title", [], "any", false, false, false, 84), "html", null, true);
        echo "\"/>
                ";
        // line 85
        echo twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 85, $this->source); })()), "introduction", [], "any", false, false, false, 85);
        echo "
                <a href=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ads_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["ad"]) || array_key_exists("ad", $context) ? $context["ad"] : (function () { throw new RuntimeError('Variable "ad" does not exist.', 86, $this->source); })()), "slug", [], "any", false, false, false, 86)]), "html", null, true);
        echo "\" class=\"btn btn-primary mt-3\">Plus d'informations</a>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "booking/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  274 => 86,  270 => 85,  264 => 84,  257 => 82,  248 => 75,  244 => 73,  241 => 72,  236 => 70,  231 => 68,  226 => 67,  223 => 66,  221 => 65,  216 => 63,  213 => 62,  210 => 61,  207 => 60,  205 => 59,  195 => 54,  191 => 53,  185 => 50,  178 => 48,  169 => 44,  160 => 38,  155 => 36,  150 => 34,  145 => 32,  140 => 30,  135 => 28,  128 => 23,  118 => 18,  109 => 14,  102 => 9,  100 => 8,  96 => 7,  93 => 6,  91 => 5,  89 => 4,  79 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block title %}Réservation n°{{booking.id}}{% endblock %}
{% block body %}
{% set ad = booking.ad %}
{% set author = ad.author %}
    <div class=\"container mat7\">
        <h1 class=\"maTmaB tac\">Votre réservation (n°{{booking.id}})</h1>
        {% if app.request.query.get('widthAlertSuccess') %}
            <div class=\"alert alert-success\">
                <h4 class=\"alert-heading\">Bravo !</h4>
                <p>
                    Votre réservation auprès de
                    <strong>
                        <a href=\"{{path('user_show', {\"slug\":author.slug})}}\">{{author.fullName}}</a>
                    </strong>
                    pour l'annonce
                    <strong>
                        <a href=\"{{path('ads_show', {\"slug\":ad.slug})}}\">{{ad.title}}</a>
                    </strong>
                </p>
            </div>
        {% endif %}
        <div class=\"row\">
            <div class=\"alert alert-light\">
                <h2>Détails</h2>
                <dl class=\"row\">
                    <dt class=\"col-md-4\">Numéro</dt>
                    <dd class=\"col-md-8\">{{booking.id}}</dd>
                    <dt class=\"col-md-4\">Date de prise en main</dt>
                    <dd class=\"col-md-8\">{{booking.startDate | date('d/m/Y')}}</dd>
                    <dt class=\"col-md-4\">Date de retour</dt>
                    <dd class=\"col-md-8\">{{booking.endDate | date('d/m/Y')}}</dd>
                    <dt class=\"col-md-4\">Nombre de jour</dt>
                    <dd class=\"col-md-8\">{{booking.duration}}</dd>
                    <dt class=\"col-md-4\">Montant total</dt>
                    <dd class=\"col-md-8\">{{booking.amount | number_format(2, ',', ' ') }}&euro;</dd>
                    <dt class=\"col-md-4\">COmmentaire</dt>
                    <dd class=\"col-md-8\">{{booking.comment | default('Aucuns commentaires') }}</dd>
                </dl> 
                <hr>
                <h2 class=\"tac alert-heading\">Votre client</h2>
                <div class=\"row mb-3\">
                    <div class=\"col-3\">
                        <img src=\"{{author.picture}}\" class=\"avatar avatar-medium\" alt=\"Avatar de {{author.fullName}}\"/>
                    </div>
                    <div class=\"col-9\">
                        <h4>
                            <a href=\"{{path('user_show', {'slug': author.slug})}}\">{{author.fullName}}</a>
                        </h4>
                        <span class=\"badge badge-primary\">{{author.ads|length}} annonces</span>
                    </div>
                </div>
                {{author.description | raw}}
                <a class=\"btn btn-primary\" href=\"{{path('user_show', {'slug':author.slug})}}\">Plus d'infos sur {{author.firstName}}</a>
            </div>

            <div class=\"alet alert-light\" id=\"comment\">
                <h2 class=\"tac alert-heading\">Votre avis compte !</h2>
                {% if date() > date(booking.endDate) %}
                    {% set comment = booking.ad.commentFromAuthor(app.user) %}
                    {% if comment is not null %}
                        <blockquote>
                            {{comment.content}}
                        </blockquote>
                        <strong>Note : </strong> {% include 'partials/rating.html.twig' with {'rating': comment.rating} %}
                    {% else %}
                        {{ form_start(form) }}
                        {{ form_widget(form) }}
                        <button type=\"submit\" class=\"btn btn-success\">Confirmer !</button>
                        {{ form_end(form) }}
                    {% endif %}
                {% else %}
                <p>Vous ne pouvez pas noté cette annonce tant que vous n'avez pas rendu le produit !</p>
                {% endif %}
            </div>

        </div>
        <div class=\"col\">
            <div class=\"alert alert-light\">
                <h2 class=\"tac alert-heading\">Votre location</h2>
                <h4>
                    <a href=\"{{path('ads_show', {'slug': author.slug})}}\">{{ad.title}}</a>
                </h4>
                <img src=\"{{ad.coverImage}}\" class=\"img-fluid\" alt=\"Image de {{ad.title}}\"/>
                {{ad.introduction | raw}}
                <a href=\"{{path('ads_show', {'slug': ad.slug})}}\" class=\"btn btn-primary mt-3\">Plus d'informations</a>
            </div>
        </div>
    </div>
{% endblock %}", "booking/show.html.twig", "C:\\Users\\esaintus\\Documents\\projetCours\\i2i\\templates\\booking\\show.html.twig");
    }
}
