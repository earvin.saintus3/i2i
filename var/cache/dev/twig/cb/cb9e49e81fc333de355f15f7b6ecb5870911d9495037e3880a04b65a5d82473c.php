<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/dashboard/index.html.twig */
class __TwigTemplate_e875c3b56425debe8d2aa25e5ec50697f61ce162d17d8f3c900ef319b9db39ce extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/dashboard/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/dashboard/index.html.twig"));

        $this->parent = $this->loadTemplate("admin/base.html.twig", "admin/dashboard/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Tableau de bord";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
<div class=\"container-fluid\">
    <h1 class=\"maTmaB tac\">Tableau de bord</h1>  
    <div class=\"row\">
        <div class=\"col\">
            <div class=\"card bg-primary text-white mb-3 text-center\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col-4\">
                            <i class=\"fas fa-users fa-3x\"></i>  
                        </div>
                        <div class=\"col-8\">
                            <h4 class=\"card-title\">";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 18, $this->source); })()), "users", [], "any", false, false, false, 18), "html", null, true);
        echo " utilisateurs</h4>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col\">
            <div class=\"card bg-dark text-white mb-3 text-center\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col-4\">
                            <i class=\"fas fa-hotel fa-3x\"></i>  
                        </div>
                        <div class=\"col-8\">
                            <h4 class=\"card-title\">";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 32, $this->source); })()), "ads", [], "any", false, false, false, 32), "html", null, true);
        echo " annonces</h4>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col\">
            <div class=\"card bg-warning text-white mb-3 text-center\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col-4\">
                            <i class=\"fas fa-folder fa-3x\"></i>  
                        </div>
                        <div class=\"col-8\">
                            <h4 class=\"card-title\">";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 46, $this->source); })()), "bookings", [], "any", false, false, false, 46), "html", null, true);
        echo " réservations</h4>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col\">
            <div class=\"card bg-success text-white mb-3 text-center\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col-4\">
                            <i class=\"fas fa-comments fa-3x\"></i>  
                        </div>
                        <div class=\"col-8\">
                            <h4 class=\"card-title\">";
        // line 60
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["stats"]) || array_key_exists("stats", $context) ? $context["stats"] : (function () { throw new RuntimeError('Variable "stats" does not exist.', 60, $this->source); })()), "comments", [], "any", false, false, false, 60), "html", null, true);
        echo " avis</h4>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col\">
                <div class=\"alert alert-danger\">
                    <h2 class=\"tac alert alert-success\"><i class=\"fas fa-arrow-up\"></i>Les annonces populaires</h2>

                    <table class=\"table table-hover\">
                        <thead>
                            <th>Annonce</th>
                            <th>Proprio</th>
                            <th>Moyenne</th>
                        </thead>
                        <tbody>
                            ";
        // line 78
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["bestAds"]) || array_key_exists("bestAds", $context) ? $context["bestAds"] : (function () { throw new RuntimeError('Variable "bestAds" does not exist.', 78, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["ad"]) {
            // line 79
            echo "                            <tr>
                                <td>";
            // line 80
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ad"], "title", [], "any", false, false, false, 80), "html", null, true);
            echo "</td>
                                <td>
                                    <img src=\"";
            // line 82
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ad"], "picture", [], "any", false, false, false, 82), "html", null, true);
            echo "\" alt=\"\" class=\"avatar avatar-micro\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ad"], "firstName", [], "any", false, false, false, 82), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ad"], "lastName", [], "any", false, false, false, 82), "html", null, true);
            echo "
                                </td>
                                <td class=\"text-center\">
                                    <span class=\"badge badge-dark\">
                                        ";
            // line 86
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ad"], "note", [], "any", false, false, false, 86), 1, ",", " "), "html", null, true);
            echo "
                                    </span>
                                </td>
                            </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 91
        echo "                        </tbody>
                    </table>
                </div>
            </div>
            <div class=\"col\">
                <div class=\"alert alert-success\">
                    <h2 class=\"tac alert alert-success\"><i class=\"fas fa-arrow-down\"></i>Les moins populaires</h2>

                    <table class=\"table table-hover\">
                        <thead>
                            <th>Annonce</th>
                            <th>Proprio</th>
                            <th>Moyenne</th>
                        </thead>
                        <tbody>
                            ";
        // line 106
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["worstAds"]) || array_key_exists("worstAds", $context) ? $context["worstAds"] : (function () { throw new RuntimeError('Variable "worstAds" does not exist.', 106, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["ad"]) {
            // line 107
            echo "                            <tr>
                                <td>";
            // line 108
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ad"], "title", [], "any", false, false, false, 108), "html", null, true);
            echo "</td>
                                <td>
                                    <img src=\"";
            // line 110
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ad"], "picture", [], "any", false, false, false, 110), "html", null, true);
            echo "\" alt=\"\" class=\"avatar avatar-micro\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ad"], "firstName", [], "any", false, false, false, 110), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ad"], "lastName", [], "any", false, false, false, 110), "html", null, true);
            echo "
                                </td>
                                <td class=\"text-center\">
                                    <span class=\"badge badge-dark\">
                                        ";
            // line 114
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ad"], "note", [], "any", false, false, false, 114), 1, ",", " "), "html", null, true);
            echo "
                                    </span>
                                </td>
                            </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 119
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/dashboard/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  259 => 119,  248 => 114,  237 => 110,  232 => 108,  229 => 107,  225 => 106,  208 => 91,  197 => 86,  186 => 82,  181 => 80,  178 => 79,  174 => 78,  153 => 60,  136 => 46,  119 => 32,  102 => 18,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin/base.html.twig' %}

{% block title %}Tableau de bord{% endblock %}

{% block body %}

<div class=\"container-fluid\">
    <h1 class=\"maTmaB tac\">Tableau de bord</h1>  
    <div class=\"row\">
        <div class=\"col\">
            <div class=\"card bg-primary text-white mb-3 text-center\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col-4\">
                            <i class=\"fas fa-users fa-3x\"></i>  
                        </div>
                        <div class=\"col-8\">
                            <h4 class=\"card-title\">{{ stats.users }} utilisateurs</h4>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col\">
            <div class=\"card bg-dark text-white mb-3 text-center\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col-4\">
                            <i class=\"fas fa-hotel fa-3x\"></i>  
                        </div>
                        <div class=\"col-8\">
                            <h4 class=\"card-title\">{{ stats.ads }} annonces</h4>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col\">
            <div class=\"card bg-warning text-white mb-3 text-center\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col-4\">
                            <i class=\"fas fa-folder fa-3x\"></i>  
                        </div>
                        <div class=\"col-8\">
                            <h4 class=\"card-title\">{{ stats.bookings }} réservations</h4>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col\">
            <div class=\"card bg-success text-white mb-3 text-center\">
                <div class=\"card-body\">
                    <div class=\"row\">
                        <div class=\"col-4\">
                            <i class=\"fas fa-comments fa-3x\"></i>  
                        </div>
                        <div class=\"col-8\">
                            <h4 class=\"card-title\">{{ stats.comments }} avis</h4>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col\">
                <div class=\"alert alert-danger\">
                    <h2 class=\"tac alert alert-success\"><i class=\"fas fa-arrow-up\"></i>Les annonces populaires</h2>

                    <table class=\"table table-hover\">
                        <thead>
                            <th>Annonce</th>
                            <th>Proprio</th>
                            <th>Moyenne</th>
                        </thead>
                        <tbody>
                            {% for ad in bestAds %}
                            <tr>
                                <td>{{ ad.title }}</td>
                                <td>
                                    <img src=\"{{ad.picture}}\" alt=\"\" class=\"avatar avatar-micro\">{{ad.firstName}} {{ad.lastName}}
                                </td>
                                <td class=\"text-center\">
                                    <span class=\"badge badge-dark\">
                                        {{ad.note | number_format(1, \",\", \" \")}}
                                    </span>
                                </td>
                            </tr>
                            {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
            <div class=\"col\">
                <div class=\"alert alert-success\">
                    <h2 class=\"tac alert alert-success\"><i class=\"fas fa-arrow-down\"></i>Les moins populaires</h2>

                    <table class=\"table table-hover\">
                        <thead>
                            <th>Annonce</th>
                            <th>Proprio</th>
                            <th>Moyenne</th>
                        </thead>
                        <tbody>
                            {% for ad in worstAds %}
                            <tr>
                                <td>{{ ad.title }}</td>
                                <td>
                                    <img src=\"{{ad.picture}}\" alt=\"\" class=\"avatar avatar-micro\">{{ad.firstName}} {{ad.lastName}}
                                </td>
                                <td class=\"text-center\">
                                    <span class=\"badge badge-dark\">
                                        {{ad.note | number_format(1, \",\", \" \")}}
                                    </span>
                                </td>
                            </tr>
                            {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{% endblock %}
", "admin/dashboard/index.html.twig", "C:\\Users\\esaintus\\Documents\\projetCours\\i2i\\templates\\admin\\dashboard\\index.html.twig");
    }
}
