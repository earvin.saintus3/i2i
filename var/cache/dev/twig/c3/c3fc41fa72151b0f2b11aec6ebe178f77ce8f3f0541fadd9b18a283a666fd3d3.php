<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home.html.twig */
class __TwigTemplate_7a7120e0ecabe8b60ac4cb7034ca2a10f9200a3e6eb877d5ee40d32b53704bf9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    High-tech to High-tech
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "    <!-- ***** Welcome Area Start ***** -->
    <div class=\"welcome-area\" id=\"welcome\">
        <!-- ***** Header Text Start ***** -->
        <div class=\"header-text\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"left-text col-lg-6 col-md-12 col-sm-12 col-xs-12\"
                        data-scroll-reveal=\"enter left move 30px over 0.6s after 0.4s\">
                        <h1><em>i2i</em></h1>
                        <h2 class=\"weightBold\">High-Tech to High-tech</h2>
                        <p>Vous poss&eacute;der plusieurs &eacute;quiepements informatique ou souhaitez en louer sur une p&eacute;riode d&eacute;termin&eacute; plutôt que de l'acheter ?</p> 
                        <a href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ads_index");
        echo "\" class=\"main-button-slider\">Voir nos annonces</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
    <!-- ***** Welcome Area End ***** -->
    <!-- ***** Features Big Item Start ***** -->
    <section class=\"section\" id=\"about\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-4 col-md-6 col-sm-12 col-xs-12\"
                    data-scroll-reveal=\"enter left move 30px over 0.6s after 0.4s\">
                    <div class=\"features-item\">
                        <div class=\"features-icon\">
                            <h2>01</h2>
                            <img src=\"/images/features-icon-1.png\" alt=\"\">
                            <h4>Analyse des annonces</h4>
                            <p>Nous optimisons nos crit&egrave;res pour vous proposez ce qu'il y a de mieux.</p>
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-4 col-md-6 col-sm-12 col-xs-12\"
                    data-scroll-reveal=\"enter bottom move 30px over 0.6s after 0.4s\">
                    <div class=\"features-item\">
                        <div class=\"features-icon\">
                            <h2>02</h2>
                            <img src=\"/images/features-icon-2.png\" alt=\"\">
                            <h4>Localisation</h4>
                            <p>Nous selectionnons les meilleures annonces prêt de chez vous.</p>
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-4 col-md-6 col-sm-12 col-xs-12\"
                    data-scroll-reveal=\"enter right move 30px over 0.6s after 0.4s\">
                    <div class=\"features-item\">
                        <div class=\"features-icon\">
                            <h2>03</h2>
                            <img src=\"/images/features-icon-3.png\" alt=\"\">
                            <h4>Service client</h4>
                            <p>Nous r&eacute;pondons à toutes vos questions dans les plus bref d&eacute;lai.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->
    <div class=\"left-image-decor\"></div>
    <!-- ***** Features Big Item Start ***** -->
    <section class=\"section\" id=\"promotion\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"left-image col-lg-5 col-md-12 col-sm-12 mobile-bottom-fix-big\"
                    data-scroll-reveal=\"enter left move 30px over 0.6s after 0.4s\">
                    <img src=\"/images/left-image.png\" class=\"rounded img-fluid d-block mx-auto\" alt=\"App\">
                </div>
                <div class=\"right-text offset-lg-1 col-lg-6 col-md-12 col-sm-12 mobile-bottom-fix\">
                    <ul>
                        <li data-scroll-reveal=\"enter right move 30px over 0.6s after 0.4s\">
                            <img src=\"/images/about-icon-01.png\" alt=\"\">
                            <div class=\"text\">
                                <h4>Titre 1 à d&eacute;finir</h4>
                                <p>Le Lorem Ipsum est simplement du faux texte employ&eacute; dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte</p>
                            </div>
                        </li>
                        <li data-scroll-reveal=\"enter right move 30px over 0.6s after 0.5s\">
                            <img src=\"/images/about-icon-02.png\" alt=\"\">
                            <div class=\"text\">
                                <h4>Titre 2 à d&eacute;finir</h4>
                                <p>Le Lorem Ipsum est simplement du faux texte employ&eacute; dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte</p>
                            </div>
                        </li>
                        <li data-scroll-reveal=\"enter right move 30px over 0.6s after 0.6s\">
                            <img src=\"/images/about-icon-03.png\" alt=\"\">
                            <div class=\"text\">
                                <h4>Titre 3 à d&eacute;finir</h4>
                                <p>Le Lorem Ipsum est simplement du faux texte employ&eacute; dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte</p>    
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->
    <div class=\"right-image-decor\"></div>
    <!-- ***** Testimonials Starts ***** -->
    <section class=\"section\" id=\"testimonials\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-8 offset-lg-2\">
                    <div class=\"center-heading\">
                        <h2>Nos meilleurs <em>annonces</em></h2>
                    </div>
                </div>
                <div class=\"col-lg-10 col-md-12 col-sm-12 mobile-bottom-fix-big\"
                    data-scroll-reveal=\"enter left move 30px over 0.6s after 0.4s\">
                    <div class=\"owl-carousel owl-theme\">
                        ";
        // line 118
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ads"]) || array_key_exists("ads", $context) ? $context["ads"] : (function () { throw new RuntimeError('Variable "ads" does not exist.', 118, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 119
            echo "                            ";
            $this->loadTemplate("ad/_adHome.html.twig", "home.html.twig", 119)->display(twig_array_merge($context, ["ad" => twig_get_attribute($this->env, $this->source, $context["data"], "annonce", [], "any", false, false, false, 119)]));
            // line 120
            echo "                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 121
        echo "                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class=\"section\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-8 offset-lg-2\">
                    <div class=\"center-heading\">
                        <h2 class=\"tac h1 my-5\">Nos propriétaires stars !</h2>
                    </div>
                </div>
                <div class=\"col-lg-10 col-md-12 col-sm-12 mobile-bottom-fix-big\"
                    data-scroll-reveal=\"enter left move 30px over 0.6s after 0.4s\">
                    <div class=\"owl-carousel owl-theme\">
                        ";
        // line 137
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 137, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 138
            echo "                        <div class=\"item service-item\">
                            <div class=\"testimonial-content\">
                                <img src=\"";
            // line 140
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "user", [], "any", false, false, false, 140), "picture", [], "any", false, false, false, 140), "html", null, true);
            echo "\" alt=\"Avatar de ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "user", [], "any", false, false, false, 140), "fullName", [], "any", false, false, false, 140), "html", null, true);
            echo "\" class=\"float-left mr-3 picture\">
                                <h4 class=\"card-title\">";
            // line 141
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "user", [], "any", false, false, false, 141), "fullName", [], "any", false, false, false, 141), "html", null, true);
            echo "</h4>
                                <p>";
            // line 142
            echo twig_truncate_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "user", [], "any", false, false, false, 142), "introduction", [], "any", false, false, false, 142), 150);
            echo "</p>
                                <a href=\"";
            // line 143
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_show", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["data"], "user", [], "any", false, false, false, 143), "slug", [], "any", false, false, false, 143)]), "html", null, true);
            echo "\" class=\"btn btn-warning float-right\">En savoir plus !</a>
                            </div>
                        </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 147
        echo "                    </div> 
                </div>
            </div>
        </div>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  290 => 147,  280 => 143,  276 => 142,  272 => 141,  266 => 140,  262 => 138,  258 => 137,  240 => 121,  226 => 120,  223 => 119,  206 => 118,  103 => 18,  90 => 7,  80 => 6,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
    
{% block title %}
    High-tech to High-tech
{% endblock %}
{% block body %}
    <!-- ***** Welcome Area Start ***** -->
    <div class=\"welcome-area\" id=\"welcome\">
        <!-- ***** Header Text Start ***** -->
        <div class=\"header-text\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"left-text col-lg-6 col-md-12 col-sm-12 col-xs-12\"
                        data-scroll-reveal=\"enter left move 30px over 0.6s after 0.4s\">
                        <h1><em>i2i</em></h1>
                        <h2 class=\"weightBold\">High-Tech to High-tech</h2>
                        <p>Vous poss&eacute;der plusieurs &eacute;quiepements informatique ou souhaitez en louer sur une p&eacute;riode d&eacute;termin&eacute; plutôt que de l'acheter ?</p> 
                        <a href=\"{{ path('ads_index') }}\" class=\"main-button-slider\">Voir nos annonces</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
    <!-- ***** Welcome Area End ***** -->
    <!-- ***** Features Big Item Start ***** -->
    <section class=\"section\" id=\"about\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-4 col-md-6 col-sm-12 col-xs-12\"
                    data-scroll-reveal=\"enter left move 30px over 0.6s after 0.4s\">
                    <div class=\"features-item\">
                        <div class=\"features-icon\">
                            <h2>01</h2>
                            <img src=\"/images/features-icon-1.png\" alt=\"\">
                            <h4>Analyse des annonces</h4>
                            <p>Nous optimisons nos crit&egrave;res pour vous proposez ce qu'il y a de mieux.</p>
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-4 col-md-6 col-sm-12 col-xs-12\"
                    data-scroll-reveal=\"enter bottom move 30px over 0.6s after 0.4s\">
                    <div class=\"features-item\">
                        <div class=\"features-icon\">
                            <h2>02</h2>
                            <img src=\"/images/features-icon-2.png\" alt=\"\">
                            <h4>Localisation</h4>
                            <p>Nous selectionnons les meilleures annonces prêt de chez vous.</p>
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-4 col-md-6 col-sm-12 col-xs-12\"
                    data-scroll-reveal=\"enter right move 30px over 0.6s after 0.4s\">
                    <div class=\"features-item\">
                        <div class=\"features-icon\">
                            <h2>03</h2>
                            <img src=\"/images/features-icon-3.png\" alt=\"\">
                            <h4>Service client</h4>
                            <p>Nous r&eacute;pondons à toutes vos questions dans les plus bref d&eacute;lai.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->
    <div class=\"left-image-decor\"></div>
    <!-- ***** Features Big Item Start ***** -->
    <section class=\"section\" id=\"promotion\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"left-image col-lg-5 col-md-12 col-sm-12 mobile-bottom-fix-big\"
                    data-scroll-reveal=\"enter left move 30px over 0.6s after 0.4s\">
                    <img src=\"/images/left-image.png\" class=\"rounded img-fluid d-block mx-auto\" alt=\"App\">
                </div>
                <div class=\"right-text offset-lg-1 col-lg-6 col-md-12 col-sm-12 mobile-bottom-fix\">
                    <ul>
                        <li data-scroll-reveal=\"enter right move 30px over 0.6s after 0.4s\">
                            <img src=\"/images/about-icon-01.png\" alt=\"\">
                            <div class=\"text\">
                                <h4>Titre 1 à d&eacute;finir</h4>
                                <p>Le Lorem Ipsum est simplement du faux texte employ&eacute; dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte</p>
                            </div>
                        </li>
                        <li data-scroll-reveal=\"enter right move 30px over 0.6s after 0.5s\">
                            <img src=\"/images/about-icon-02.png\" alt=\"\">
                            <div class=\"text\">
                                <h4>Titre 2 à d&eacute;finir</h4>
                                <p>Le Lorem Ipsum est simplement du faux texte employ&eacute; dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte</p>
                            </div>
                        </li>
                        <li data-scroll-reveal=\"enter right move 30px over 0.6s after 0.6s\">
                            <img src=\"/images/about-icon-03.png\" alt=\"\">
                            <div class=\"text\">
                                <h4>Titre 3 à d&eacute;finir</h4>
                                <p>Le Lorem Ipsum est simplement du faux texte employ&eacute; dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte</p>    
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->
    <div class=\"right-image-decor\"></div>
    <!-- ***** Testimonials Starts ***** -->
    <section class=\"section\" id=\"testimonials\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-8 offset-lg-2\">
                    <div class=\"center-heading\">
                        <h2>Nos meilleurs <em>annonces</em></h2>
                    </div>
                </div>
                <div class=\"col-lg-10 col-md-12 col-sm-12 mobile-bottom-fix-big\"
                    data-scroll-reveal=\"enter left move 30px over 0.6s after 0.4s\">
                    <div class=\"owl-carousel owl-theme\">
                        {% for data in ads %}
                            {% include 'ad/_adHome.html.twig' with {'ad': data.annonce} %}
                        {% endfor %}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class=\"section\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-8 offset-lg-2\">
                    <div class=\"center-heading\">
                        <h2 class=\"tac h1 my-5\">Nos propriétaires stars !</h2>
                    </div>
                </div>
                <div class=\"col-lg-10 col-md-12 col-sm-12 mobile-bottom-fix-big\"
                    data-scroll-reveal=\"enter left move 30px over 0.6s after 0.4s\">
                    <div class=\"owl-carousel owl-theme\">
                        {% for data in users %}
                        <div class=\"item service-item\">
                            <div class=\"testimonial-content\">
                                <img src=\"{{data.user.picture}}\" alt=\"Avatar de {{data.user.fullName}}\" class=\"float-left mr-3 picture\">
                                <h4 class=\"card-title\">{{data.user.fullName}}</h4>
                                <p>{{data.user.introduction | truncate(150) | raw }}</p>
                                <a href=\"{{ path('user_show', {'slug':data.user.slug}) }}\" class=\"btn btn-warning float-right\">En savoir plus !</a>
                            </div>
                        </div>
                        {% endfor %}
                    </div> 
                </div>
            </div>
        </div>
    </section>
{% endblock %}", "home.html.twig", "C:\\Users\\esaintus\\Documents\\projetCours\\i2i\\templates\\home.html.twig");
    }
}
