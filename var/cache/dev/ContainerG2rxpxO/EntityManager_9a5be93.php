<?php

namespace ContainerG2rxpxO;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder7b1af = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerfb350 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesb3ac5 = [
        
    ];

    public function getConnection()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getConnection', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getMetadataFactory', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getExpressionBuilder', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'beginTransaction', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getCache', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getCache();
    }

    public function transactional($func)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'transactional', array('func' => $func), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->transactional($func);
    }

    public function commit()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'commit', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->commit();
    }

    public function rollback()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'rollback', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getClassMetadata', array('className' => $className), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'createQuery', array('dql' => $dql), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'createNamedQuery', array('name' => $name), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'createQueryBuilder', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'flush', array('entity' => $entity), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'clear', array('entityName' => $entityName), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->clear($entityName);
    }

    public function close()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'close', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->close();
    }

    public function persist($entity)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'persist', array('entity' => $entity), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'remove', array('entity' => $entity), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'refresh', array('entity' => $entity), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'detach', array('entity' => $entity), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'merge', array('entity' => $entity), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getRepository', array('entityName' => $entityName), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'contains', array('entity' => $entity), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getEventManager', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getConfiguration', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'isOpen', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getUnitOfWork', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getProxyFactory', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'initializeObject', array('obj' => $obj), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'getFilters', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'isFiltersStateClean', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'hasFilters', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return $this->valueHolder7b1af->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerfb350 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder7b1af) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder7b1af = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder7b1af->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, '__get', ['name' => $name], $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        if (isset(self::$publicPropertiesb3ac5[$name])) {
            return $this->valueHolder7b1af->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder7b1af;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder7b1af;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder7b1af;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder7b1af;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, '__isset', array('name' => $name), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder7b1af;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder7b1af;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, '__unset', array('name' => $name), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder7b1af;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder7b1af;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, '__clone', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        $this->valueHolder7b1af = clone $this->valueHolder7b1af;
    }

    public function __sleep()
    {
        $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, '__sleep', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;

        return array('valueHolder7b1af');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerfb350 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerfb350;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerfb350 && ($this->initializerfb350->__invoke($valueHolder7b1af, $this, 'initializeProxy', array(), $this->initializerfb350) || 1) && $this->valueHolder7b1af = $valueHolder7b1af;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder7b1af;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder7b1af;
    }


}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
