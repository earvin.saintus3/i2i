<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerG2rxpxO\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerG2rxpxO/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerG2rxpxO.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerG2rxpxO\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerG2rxpxO\App_KernelDevDebugContainer([
    'container.build_hash' => 'G2rxpxO',
    'container.build_id' => 'f7906534',
    'container.build_time' => 1632315081,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerG2rxpxO');
